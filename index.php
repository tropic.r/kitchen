<?php


try{

    require_once __DIR__."/init.php";
    require_once _CONFIG."routes.php";
    require_once _EXTRA."router.php";

    $router = new Router($routes);
    $container = new container();
    $container->env();

}catch (Exception $e){

    echo $e->getMessage();

    exit();
}