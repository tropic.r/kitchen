<?php

use Symfony\Component\Mailer\Transport;
use Symfony\Component\Mailer\Mailer;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class container
{


    public function twig()
    {

        $loader = new \Twig\Loader\FilesystemLoader(_TEMPLATES);
        $twig = new \Twig\Environment($loader);

        return $twig;

    }


    public function env()
    {
        $dotenv = Dotenv\Dotenv::createImmutable(_CONFIG);
        $dotenv->load();
    }


    public function mailer()
    {
        $transport = Transport::fromDsn('smtp://localhost');
        $mailer = new Mailer($transport);

        $email = (new Email())
            ->from('hello@example.com')
            ->to('you@example.com')
            //->cc('cc@example.com')
            //->bcc('bcc@example.com')
            //->replyTo('fabien@example.com')
            //->priority(Email::PRIORITY_HIGH)
            ->subject('Time for Symfony Mailer!')
            ->text('Sending emails is fun again!')
            ->html('<p>See Twig integration for better HTML integration!</p>');

        $mailer->send($email);
    }


    public function logger($message,$type='error')
    {

        $log = new Logger('logger');
        $log->pushHandler(new StreamHandler(_LOGS.'logs.ini', Logger::WARNING));


        if($type==='error'){
            $log->error('Bar');
        }else{
            $log->warning('Foo');
        }

    }
}