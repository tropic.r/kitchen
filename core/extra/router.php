<?php


class Router
{
    private $uri = false;
    private $sections = false;
    private $routes = false;

    private $class = false;
    private $method = false;
    private $controller = false;


    public function __construct($routes)
    {
        $this->routes = $routes;
        $this->getUri();
        $this->checkUri();
        $this->switchPage();
    }


    private function getUri()
    {
        $this->uri = trim(parse_url($_SERVER["REQUEST_URI"], PHP_URL_PATH), "/");
    }


    private function checkUri()
    {

        if (empty($this->uri)) {
            $this->defaultPage();
        } else {
            $this->otherPage();
        }
    }


    private function defaultPage()
    {
        $array = explode("|", $this->routes['default']);
        $this->class = $array[0];
        $this->controller = str_replace("\\", "/", $array[0]) . ".php";
        $this->method = $array[1];
    }


    private function otherPage()
    {

        $this->sections = explode("/", $this->uri);

        if (count($this->sections) < 3) {
            if (isset($this->routes[$this->uri])) {
                $array = explode("|", $this->routes[$this->uri]);
                $this->class = $array[0];
                $this->controller = str_replace("\\", "/", $array[0]) . ".php";
                $this->method = $array[1];
            } else {
                $this->error();
            }

        } else if (count($this->sections) == 3) {

            $this->uri = $this->sections[0] . "/" . $this->sections[1] . "/.*";

            define("_ARGUMENT", $this->sections[2]);

            if (isset($this->routes[$this->uri])) {
                $array = explode("|", $this->routes[$this->uri]);
                $this->class = $array[0];
                $this->controller = str_replace("\\", "/", $array[0]) . ".php";
                $this->method = $array[1];
            } else {
                $this->error();
            }
        }

    }


    private function error()
    {
        $array = explode("|", $this->routes[404]);
        $this->class = $array[0];
        $this->controller = str_replace("\\", "/", $array[0]) . ".php";
        $this->method = $array[1];
    }


    private function switchPage()
    {
        $class = $this->class;
        $method = $this->method;
        $controller = $this->controller;

        if (file_exists($controller)) {

            require_once $controller;

            if (class_exists($class) && method_exists($class, $method)) {
                $c = new $class;
                $c->$method();
            } else {
                throw new Exception('class or method not found');
            }

        } else {
            throw new Exception('Controller not found');
        }
    }


}
