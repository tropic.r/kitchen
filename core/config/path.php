<?php


define("_ROOT",$_SERVER["DOCUMENT_ROOT"]."/");
define("_CORE",_ROOT."/core/");
define("_CONFIG",_ROOT."/core/config/");
define("_CONTROLLERS",_ROOT."/core/controllers/");
define("_EXTRA",_ROOT."/core/extra/");
define("_INTERFACES",_ROOT."/core/interfaces/");
define("_MODELS",_ROOT."/core/models/");
define("_PLUGINS",_ROOT."/core/plugins/");
define("_EMAILS",_ROOT."/emails/");
define("_LOGS",_ROOT."/logs/");
define("_MODULES",_ROOT."/modules/");
define("_PUBLIC",_ROOT."/public/");
define("_TEMPLATES",_ROOT."/templates/");
define("_UPLOADS",_ROOT."/uploads/");
