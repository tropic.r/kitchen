<?php
namespace core\controllers;

use const PHPSTORM_META\ANY_ARGUMENT;

class main extends \container {
    public function index()
    {
        if(defined("_ARGUMENT")) {
            echo _ARGUMENT;
        }

        echo $this->twig()->render('main/login.html.twig');
    }
}