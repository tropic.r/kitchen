function keyBoard(lang='en',size='small') {
    let engSmall = ['1','2','3','4','5','6','7','8','9','0','-','+','<<',
        'q','w','e','r','t','y','u','i','o','p','[',']','?',
        'a','s','d','f','g','h','j','k','l',';','\'','\\','|',
        'z','x','c','v','b','n','m',',','.','/','<','>','*',
        '@']

    let engBig = ['1','2','3','4','5','6','7','8','9','0','-','+','<<',
        'Q','W','E','R','T','Y','U','I','O','P','[',']','?',
        'A','S','D','F','G','H','J','K','L',';','\'','\\','|',
        'Z','X','C','V','B','N','M',',','.','/','<','>','*',
        '@']
    let display = document.querySelector('.pin-box-display')

    let div = document.createElement('div')
    div.classList.add('virtual-keyboard')
    div.style.width = '70%'
    div.style.position = 'absolute'
    div.style.left = '50%'
    div.style.bottom = '0%'
    div.style.transform = 'translate(-50%,0%)'
    div.style.zIndex = '666'
    div.style.background = '#FFF'
    div.style.boxShadow = '0 0 15px #333'
    div.style.display = 'flex'
    div.style.flexWrap = 'wrap'
    div.style.padding = '15px'
    div.style.justifyContent = 'space-between'
    div.style.gap = '2px'

    let close = document.createElement('a')
    close.innerHTML = 'закрыть'
    close.style.position = 'absolute'
    close.style.right = '-30px'
    close.style.top = '-30px'
    close.style.cursor = 'pointer'


    close.addEventListener('click',()=>{
        div.remove()
    })

    div.append(close)


    if(lang=='en' && size=='small'){
        engSmall.forEach(key=>{
            let b = document.createElement('button')
            b.innerHTML = key


            b.addEventListener('click',function () {

                if(this.innerHTML!='&lt;&lt;') {
                    display.innerHTML += this.innerHTML
                }else{
                    let pin = display.innerHTML

                    if(pin.length>0) {

                        let newWord = pin.substring(0, pin.length - 1)
                        display.innerHTML = newWord
                    }
                }
            })

            div.append(b)
        })
        let space = document.createElement('button')
        let caps = document.createElement('button')
        space.innerHTML = 'space'
        caps.innerHTML = 'изменить регистр'
        space.style.width='60%'
        div.append(caps)
        div.append(space)

        document.body.append(div)
    }


}
export default keyBoard