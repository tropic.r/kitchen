import keyBoard from './keyBoard.js'

function pinBox () {
    let pinboxButt = document.querySelector('.pin-box-buttons')

    if(pinboxButt){
        let buttonsArr = document.querySelectorAll('.pin-box-buttons button')
        let display = document.querySelector('.pin-box-display')

        buttonsArr.forEach(item=>{
            item.addEventListener('click',function(){
                if(!this.classList.contains('delete') && !this.classList.contains('sub') &&
                    !this.classList.contains('keyboard')) {
                    display.innerHTML+=this.innerHTML
                }
                else if(this.classList.contains('delete')){

                    let pin = display.innerHTML

                    if(pin.length>0) {

                        let newWord = pin.substring(0, pin.length - 1)
                        display.innerHTML = newWord
                    }
                }else if(this.classList.contains('keyboard')){
                    keyBoard()
                }
            })
        })
    }
}

export default pinBox